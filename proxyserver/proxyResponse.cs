﻿using System;
using System.Text.RegularExpressions;
using CsQuery;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Xml;


namespace proxyserver
{
    public class proxyResponse
    {
        StaticData staticData;

        public proxyResponse(StaticData staticData)
        {
            this.staticData = staticData;
        }

        private string sendRequestXML(string requestUrl) {
            string stringResponse = "";
            System.Xml.XmlNode errorXml;
            System.Xml.XmlDocument responseXml = new System.Xml.XmlDocument();

            //Replace simple Load with LoadXML + standard http request           
            try
            {
                responseXml.LoadXml(httpGetXml(requestUrl));
                errorXml = responseXml.SelectSingleNode(staticData.errorPath);
            }
            catch (Exception e)
            {
               return staticData.getStandardError(e.Message);
            }
            
            //in case the site returned error message we would like to address it and pass it on (even if it's not exception)
            if (errorXml != null && errorXml.InnerText != "OK")
            {
                return staticData.getStandardError(responseXml.SelectSingleNode(staticData.errorPath).InnerText);
             }

            foreach (XmlNode answerXPath in staticData.getRNodes("answers/answer"))
            {
                //TODO: remove "," (colon) and replace it by parameter in xml
                stringResponse += "," + responseXml.SelectSingleNode(answerXPath.InnerText).InnerText;
            }
            return stringResponse.Remove(0, 1); //remove the first colon            
        }

        private string httpGetXml(string requestUrl)
        {

            string result;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
            webRequest.Method = "GET";
            webRequest.AllowAutoRedirect = true;
            webRequest.Accept = "text/xml";
            webRequest.Headers.Add(HttpRequestHeader.CacheControl, "no-cache");
            webRequest.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.86 Safari/537.36";
            webRequest.Headers.Add(HttpRequestHeader.Pragma, "no-cache");
 
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                StreamReader responseStream = new StreamReader(webResponse.GetResponseStream());
                result = responseStream.ReadToEnd();
                //close stream and connection
                responseStream.Close();
                webResponse.Close();

                return result;
            }
            catch (Exception e)
            {

                throw e;
            }

        }


        private string sendRequestHTML(string requestUrl)
        {            
         
            CQ responseDom;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(requestUrl);
            webRequest.Method = staticData.getA("http/requestType"); //TODO: add this to xml configuration file
            webRequest.Timeout = Convert.ToInt32(staticData.getA("http/requestTimeout"));
            //TODO: try to replace this with CQ native rendering (should work)
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                StreamReader responseStream = new StreamReader(webResponse.GetResponseStream(), System.Text.Encoding.UTF8);
                //load response HTML to CsQuery DOM reader, for easy extracting of DOM elements
                responseDom = CQ.CreateDocument(responseStream);

                //close stream and connection
                responseStream.Close();
                webResponse.Close();
            }
            catch (Exception e)
            {

                return staticData.getStandardError(e.Message);
            }
                        
            //TODO : [15/07/2014 - MR] decide in what way we would like to get the response. in json or xml or maybe directly html
            //return CQ.ToJSON(processMasterElement(Convert.ToInt32(staticData.getA("resultscount")), responseDom));
            return processMasterElement(Convert.ToInt32(staticData.getA("resultsneeded")), responseDom);
        }

        private string processMasterElement(int returnThisMany, CQ dom)
        {            
            string returnHtml="";

            foreach (IDomObject masterElement in dom.Select(staticData.getAnswerMaster()).Elements)
            {
                //extract inner elements from the master element and put then inside DIVs
                returnHtml += "<div class=\"" + staticData.masterAsnwerValue + "\">"
                                //must provide masterElement as Select context, otherwise selects from the root dom element
                                //have no idea why... but fuck it
                                + masterElement.Cq().Select(staticData.getAnswerChild(), masterElement).SelectionHtml(true).Replace(",", "") 
                            + "</div>";
                //once reached desired number of outputs - return and exit
                if (--returnThisMany == 0) return returnHtml;
            }
            return returnHtml;
        }
              
        public String getResponse()
        {
            //return a list of proxy args received 
            string[] argumentsList = staticData.getProxyArguments();            

               /*parse the parameters elements, separated by predefined separator (defined in xml)*/
               //Load response xml
               if (staticData.getA("responsetype") == "xml")
               {
                   return sendRequestXML(staticData.buildRequestUrl(argumentsList));
               }

               //Load response as html
               else if (staticData.getA("responsetype") == "html")
               {
                   return sendRequestHTML(staticData.buildRequestUrl(argumentsList));
               }


           return staticData.getStandardError("could not produce a response");
       }//func
       
       }//CLASS
}//namespace