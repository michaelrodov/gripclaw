﻿using System.Xml;
using System.Collections.Generic;
using System;
using MySql.Data.MySqlClient;
using System.Net;
using System.IO;
using System.Web;


namespace proxyserver
{
    public class StaticData
    {
        readonly private string _XMLROUTEROOT;
        readonly private string _XMLAPPROOT;

        const string MASTER_SELECT_TYPE_NAME = "masterSelect";
        const string CHILD_SELECT_TYPE_NAME = "childSelect";
        
        private XmlDocument xmlDataSource;        

        private string application;
        private string routetype;
        private string[] arguments;
        
        public StaticData(string application, string routeType, string arguments)
        {
            this.application = application;
            this.routetype = routeType;  

        }

        public StaticData(string xmlDataSourcePath, string application, string routeType, string arguments)
        {
            
            /*loading static data from xml*/
            this.xmlDataSource = new XmlDocument();
            try
            {
                this.xmlDataSource.Load(xmlDataSourcePath);
            }
            catch (Exception e)
            {
                throw e;
            }

            if (application == null || routeType == null)
                throw new Exception("'application' and 'routeType' cannot be null");

            //init local vars
            this.application = application;
            this.routetype = routeType;            

            //init optimized local vars
            _XMLAPPROOT = "/routes/" + application + "/";
            _XMLROUTEROOT = _XMLAPPROOT + routetype + "/";

            validateRoutingXml();
            //remove spaces in the coordinates (waze apps can receive only coordinates)
            //spaces can cause the request to fail on waze side 
            if (application.Contains("waze"))
            {
                arguments = arguments.Replace(" ", "");
            }
            buildProxyArgumentsList(arguments);
        }

        private void buildProxyArgumentsList(string arguments)
        {
            /*parse the parameters elements, separated by predefined separator (defined in xml)*/
            try
            {
                this.arguments = arguments.Split(getA("incomingseparator").ToCharArray());
            }
            catch (Exception e)
            {
                if (getA("separator") == null)
                    throw new Exception("Parsing arguments list failed due to \"" + applicationPath + "incomingseparator\" not specified");
                else
                    throw e;
                /*If exception is caught - exit the processing of the response and return the text above*/
            }

        }

        /**
         * Insert current request and response to logging database
         * Run after request success/failure in router main function
         */
        public static void dbInsert(string response, int response_code, HttpRequest request, string application, string route, string arguments)
        {
            string ID="Not Specified";

            if (String.IsNullOrEmpty(gripclaw.Properties.Settings.Default.constr))
                return;

            MySqlConnection conn = new MySqlConnection(gripclaw.Properties.Settings.Default.constr);

            try
            {
                conn.Open();

            }
            catch (Exception e)
            {
                return;
            } 
            
            try
            {
                MySqlCommand comm = conn.CreateCommand();
                comm.CommandText = "INSERT INTO gripclaw.requests (req_application, req_route_type, req_args, response, response_code, env) VALUES(@req_application,@req_route_type,@req_args,@response,@response_code,@env)";
                comm.Parameters.AddWithValue("@req_application", application);
                comm.Parameters.AddWithValue("@req_route_type", route);
                comm.Parameters.AddWithValue("@req_args", arguments);
                comm.Parameters.AddWithValue("@response", response);
                comm.Parameters.AddWithValue("@response_code", Convert.ToInt16(response_code));

                if (request.QueryString.Get("ID") != null){
                    ID = request.QueryString.Get("ID");                  
                }

                comm.Parameters.AddWithValue("@env", ID);

                comm.ExecuteNonQuery();

            }
            catch (Exception e)
            {
                return;
            }
            finally
            {
                conn.Close();
            }


        }
        
        /// <summary>
        /// Function validates that all needed xml fields are present and are filled with correct valid values
        /// </summary>
        private void validateRoutingXml(){
            // validate that all manadatory elements present in configuration xml
            if (getR("url") == null
                || getR("requestargs") == null
                || getR("answers") == null
                || getR("answers/answer") == null)
                throw new Exception("Missing manadatory application elenement(s) (url, requestargs, answers, answers/answer), "
                                    +"or they are not placed inside application xml");

            //validate arguments parameters
            if (getR("requestargs/@argscount") == null && Convert.ToInt32(getR("requestargs/@argscount")) < 0)
                throw new Exception("Missing valid requestargs/@argscount. Returned: " + getR("requestargs/@argscount"));
            if (getR("requestargs/@argtemplate") == null)
                throw new Exception("Missing valid requestargs/@argtemplate. Returned: " + getR("requestargs/@argtemplate"));      
            
            if (getA("responsetype") == null
                || getA("resultsneeded") == null
                || getA("incomingseparator") == null
                || getA("responseerrorpath") == null)
                throw new Exception("Missing manadatory route elenement(s) (responsetype, resultsneeded, incomingseparator, responseerrorpath)");

            if (Convert.ToInt32(getA("resultsneeded")) < 0)
                    throw new Exception("Element 'resultsneeded' must be greater then 0");

            if (getA("responsetype") != "xml" && getA("responsetype") != "html")
                    throw new Exception("Element 'responsetype' must be either 'html' or 'xml'");

           
        }
        
        /// <summary>
        /// function builds a GET request to be used to query the target site
        /// this function uses parameters that are being passed to the site, and replaces the arguments in tge xml file 
        /// describing the routing of proxy request
        /// </summary>
        /// <returns>GET request string</returns>
        public string buildRequestUrl(string[] proxyArgs) {
            int argsCount = proxyArgs.Length;
            string argsTemplate = getR("requestargs/@argtemplate");
            string requestArgs = getR("requestargs");
            int inx=1;

            if (argsCount == 0) return getR("url"); //return the URL as is if there are no arguments passed

            foreach (string arg in proxyArgs)
            {
                //replace the template subs for arguments with arguments sent to proxy server by GET request
                requestArgs = requestArgs.Replace(argsTemplate + inx, arg);
                inx++;
            }
            return getR("url") + requestArgs;
        }      

        public string errorPath {
            get { return xmlDataSource.SelectSingleNode(_XMLAPPROOT + "responseerrorpath").InnerText; }
        }

        public string masterAsnwerValue {
            get { return MASTER_SELECT_TYPE_NAME; }
        }

        public string childAsnwerValue {
            get { return CHILD_SELECT_TYPE_NAME; }
        }

        public string getStandardError(string err) {
            return "error:" + err + ":";
        }

        public string getStandardTextResponse(string res)
        {
            return ":" + res + ":";
        }

        public XmlNodeList getRNodes(string arg) {
            return xmlDataSource.SelectNodes(_XMLROUTEROOT + arg);
        }

        public string getR(string arg) {
            return xmlDataSource.SelectSingleNode(_XMLROUTEROOT + arg).InnerText;
        }

        public string getAnswerMaster()
        {
            return getR("answers/answer[@type='" + MASTER_SELECT_TYPE_NAME + "']");
        }
       
        public string getAnswerChild()
        {
            return getR("answers/answer[@type='" + CHILD_SELECT_TYPE_NAME + "']");
        }
        
        public string getA(string arg)
        {
            return xmlDataSource.SelectSingleNode(_XMLAPPROOT + arg).InnerText;
        }

        public string[] getProxyArguments() {
           return this.arguments;
        }
        
        public string applicationPath {
            get { return _XMLAPPROOT; }
        }
        
        public string routePath
        {
            get { return _XMLROUTEROOT; }
        }
        /***
         * Simple function for sending and receiving internal info via internal server interfaces
         **/
        public static string internalApiHttpGet(HttpRequest request, string url)
        {
            string currDomain = request.Url.Scheme + System.Uri.SchemeDelimiter + request.Url.Host+":"+request.Url.Port;
            StreamReader responseStream;
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(currDomain+url);
            webRequest.Method = "GET";
            webRequest.Timeout = 30000;
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                responseStream = new StreamReader(webResponse.GetResponseStream(), System.Text.Encoding.UTF8);
                return responseStream.ReadToEnd();
            }
            catch (Exception e)
            {
                throw e;
            }


        }
    }
}