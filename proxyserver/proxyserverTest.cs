﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace gripclaw
{
    [TestClass] 
    public class proxyserverTest
    {
        private string testApi(string application, string routeType, string arguments)
        {
            proxyserver.StaticData staticData;
            staticData = new proxyserver.StaticData(System.Web.HttpContext.Current.Server.MapPath("~/App_Data/routing.xml"), application, routeType, arguments);
            proxyserver.proxyResponse proxyServer = new proxyserver.proxyResponse(staticData);
            return proxyServer.getResponse();
        }

        [TestMethod]
        public void testWaze()
        {
            testApi("waze-il", "time", "test");
        }
    }
}