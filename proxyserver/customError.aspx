﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="customError.aspx.cs" Inherits="proxyserver.customError" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Error</title>
</head>
<body>
    <form id="frm" runat="server">
    <div>
        <%
            string responseText = "";

            switch ((Request.Params.Get("error") == null) ? String.Empty : Request.Params.Get("error"))
            { 
                case "emptyroute" :
                    responseText = "Cannot handle an empty route";
                    break;
                case "argsmissing":
                    responseText = "Must have arguments passed";
                    break;
                case "":
                    responseText = "General error";
                    break;
                case "500":
                    responseText = "Application error 500";
                    break;
                case "403":
                    responseText = "Access denied 403";
                    break;
                case "404":
                    responseText = "Page not found 404";
                    break;
            }

            Response.Write("error:" + responseText + ":");
        %>
    </div>
    </form>
</body>
</html>
