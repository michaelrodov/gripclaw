﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" ErrorPage="~/customError.aspx"
    CodeBehind="ComplexRouter.aspx.cs" Inherits="proxyserver._Default1" %>
<%  

    /*Gathering parameters from url route*/
    string application = Page.RouteData.Values["application"] as string;
    string argFrom = Page.RouteData.Values["argFrom"] as string;
    String argTo = Page.RouteData.Values["argTo"] as string;
    
    /*Testing*/
    if (application == "waze" && argFrom == "test")
    {
        argFrom = "32.1147266,34.8426073";
    }
    if (application == "waze" && argTo == "test")
    {
        argTo = "Haifa, Israel";
    }
    
    if (application == "waze" && !String.IsNullOrEmpty(argFrom) && !String.IsNullOrEmpty(argTo))
    {
        string wazeFlavor;
        try
        {
            string routingXml = Server.MapPath("~/App_Data/routing.xml");
            //find country from geocode(argFrom)
            string fromCountry = (new proxyserver.proxyResponse(new proxyserver.StaticData(routingXml, "revgeocode", "country", argFrom))).getResponse();
            //find coordinates from revgeocode(argTo)
            string toCoordinates = (new proxyserver.proxyResponse(new proxyserver.StaticData(routingXml, "geocode", "a", argTo))).getResponse();


            if (fromCountry.Equals("United States"))
            {
                wazeFlavor = "waze-us";
            }
            else
                if (fromCountry.Equals("Israel"))
                {
                    wazeFlavor = "waze-il";
                }
                else
                {
                    wazeFlavor = "waze-row";
                }

            string coordinateArgs = argFrom + "," + toCoordinates;
            string trip = (new proxyserver.proxyResponse(new proxyserver.StaticData(routingXml, wazeFlavor, "timedist", coordinateArgs))).getResponse();
            //System.Threading.Thread.Sleep(5000); //wait between runs - azure/google/waze blocks high request rates
            //string tripDistance = (new proxyserver.proxyResponse(new proxyserver.StaticData(routingXml, wazeFlavor, "distance", coordinateArgs))).getResponse();

            int response_code = Response.StatusCode;
            //string response = trip;
            Response.Write(trip);
            //proxyserver.StaticData.dbInsert(trip, response_code, Request, "complex", "waze", argFrom + "," + argTo);
        }
        catch (Exception e)
        {
            Response.Write("error:"+e.Message+":");
            //proxyserver.StaticData.dbInsert(e.Message, 500, Request, "complex", "waze", argFrom + "," + argTo);
          
        }
    }
      
%>