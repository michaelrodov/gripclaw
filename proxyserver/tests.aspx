﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tests.aspx.cs" Inherits="gripclaw.Tests" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link type='text/css' rel='stylesheet' href='tests.css' />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>

        function get(url, goodResult, index) {
            $("#loader" + index).removeClass("invisible");

            var goodResultRegex = RegExp(goodResult,"gi");    
            $.ajax({
                type: "GET",
                url: url,
                dataType: "text",
                success: function (data, textStatus) {
                    $("#result" + index).textContent = data;
                    $("#loader" + index).addClass("invisible");
                    //if theres an exact match to required regex
                    if (data && goodResult && data.match(goodResultRegex).length  && data.match(goodResultRegex).length == 1) {
                        $("#row" + index).removeClass("failure").addClass("success");
                        
                    } else {
                        $("#row" + index).removeClass("success").addClass("failure");
                    }
                },
                failure: function (data, textStatus) {
                    $("#row" + index).removeClass("success").addClass("failure");
                }

            })
        }
    </script>
</head>
<body>
    <div>
        <%
            string frGermanyFrom = "50.111984,8.683132";
            string frGermanyTo = "50.111383,8.689515";
            string frGermanyToName = "Frankfurt, Germany";

            string laCaliforniaFrom = "34.045186,-118.253562";
            string laCaliforniaTo = "34.044169,-118.243924";
            string laCaliforniaToName = "Los Angeles California US";

            string taIsraelFrom = "32.083584,34.789114";
            string taIsraelTo = "32.088479,34.780262";
            string taIsraelToName = "Alenbi 44, Tel Aviv, Israel";
            
            string bgIndiaFrom = "12.967456, 77.588784";
            string bgIndiaToName = "Bengaluru, Karnataka, India";
            
            string BeijingFrom = "39.899418, 116.371481";
            string BeijingToName = "Xicheng Qu, Beijing Sh, China";

            string matchTime = "^[0-9]*$";
            string matchTimeDist = "^[0-9]*,[0-9]*$";
            string matchCoordinates = "^[0-9]*&#46;[0-9]*,[0-9]*&#46;[0-9]*$";
            string goodResult = "^OK$";
            ArrayList requests = new ArrayList();
            requests.Add(new string[] { "/geocode/a/israel?ID=Testing", matchCoordinates, "Israel Maps Geocode" });
            requests.Add(new string[] { "/revgeocode/country/31.0460510,34.8516120?ID=Testing", "Israel", "Israel Maps ReVGeo" });
            requests.Add(new string[] { "/waze-il/time/" + taIsraelFrom + "," + taIsraelTo + "?ID=Testing", matchTime, "waze-il time TA-TA" });
            requests.Add(new string[] { "/waze-il/timedist/" + taIsraelFrom + "," + taIsraelTo + "?ID=Testing", matchTimeDist, "waze-il timedist TA-TA" });
            requests.Add(new string[] { "/waze-il/distance/" + taIsraelFrom + "," + taIsraelTo + "?ID=Testing", matchTime, "waze-il distance TA-TA" });
            requests.Add(new string[] { "/waze-us/time/" + laCaliforniaFrom + "," + laCaliforniaTo + "?ID=Testing", matchTime, "waze-us time LA.California" });
            requests.Add(new string[] { "/waze-us/timedist/" + laCaliforniaFrom + "," + laCaliforniaTo + "?ID=Testing", matchTimeDist, "waze-us timedist LA.California" });
            requests.Add(new string[] { "/waze-us/distance/" + laCaliforniaFrom + "," + laCaliforniaTo + "?ID=Testing", matchTime, "waze-us distance LA.California" });
            requests.Add(new string[] { "/waze-row/time/" + frGermanyFrom + "," + frGermanyTo + "?ID=Testing", matchTime, "waze-row time Frankfurt Germany" });
            requests.Add(new string[] { "/waze-row/timedist/" + frGermanyFrom + "," + frGermanyTo + "?ID=Testing", matchTimeDist, "waze-row timedist Frankfurt Germany" });
            requests.Add(new string[] { "/waze-row/distance/" + frGermanyFrom + "," + frGermanyTo + "?ID=Testing", matchTime, "waze-row distance Frankfurt Germany" });
            requests.Add(new string[] { "/complex/waze/" + taIsraelFrom + "/" + taIsraelToName + "?ID=Testing", matchTimeDist, "complex waze TA Israel" });
            requests.Add(new string[] { "/complex/waze/" + bgIndiaFrom + "/" + bgIndiaToName + "?ID=Testing", matchTimeDist, "complex waze TA Bungalore India" });
            requests.Add(new string[] { "/complex/waze/" + frGermanyFrom + "/" + frGermanyToName + "?ID=Testing", matchTimeDist, "complex waze Frankfurt Germany" });
            requests.Add(new string[] { "/complex/waze/" + BeijingFrom + "/" + BeijingToName + "?ID=Testing", matchTimeDist, "complex waze Beijing China" });
            requests.Add(new string[] { "/complex/waze/" + laCaliforniaFrom + "/" + laCaliforniaToName + "?ID=Testing", matchTimeDist, "complex waze LA.California" });
            requests.Add(new string[] { "/log/info?c=information%20logging%20test&ID=Testing", goodResult, "Info Logging test" });
            requests.Add(new string[] { "/log/error?c=error%20logging%20test&ID=Testing", goodResult, "Error Logging test" });
            requests.Add(new string[] { "/log/debug?c=08-01-2016%2023.30,%201452288652%20%5Btask=___testInfra:0%5D%20tesing%20logging%20function&ID=Testing", goodResult, "Debug Logging test" });

            /**
             * A trick to engage the session object by accessing a session id
             * To avoid “Session state has created a session id, but cannot save it because the response was already flushed by the application.”
             * That happens because sometimes the SessionID is not set in the cookie when Session_Srtart event executes in the global asax.
             * You encounter this error because at somepoint in the page life cycle a variable is set in the session. 
             * After the request ends, ASP.NET tries to set the SessionID too, but if the Request was flushed and this exception will be thrown.
             * Source: http://stackoverflow.com/questions/904952/whats-causing-session-state-has-created-a-session-id-but-cannot-save-it-becau
             */
            //string sessionId = Session.SessionID; //DO NOT REMOVE - Read above
            double inx=1;
            
            //loop through all interfaces and build html with results
            foreach(string[] request in requests){
                Response.Write("<div class='rows' id='row"+inx+"'>"
                                + "<div id='loader" + inx + "' class=\'invisible cssload-loader\'><div class='cssload-inner cssload-one'></div><div class='cssload-inner cssload-two'></div><div class='cssload-inner cssload-three'></div></div>"
                                + "<div class='divbutton' onclick='get(\""+request[0]+"\",\""+request[1]+"\","+inx+")'>" + request[2] + "</div>" 
                                + "<p id='result"+inx+"'></p>"
                              + "</div><br />");
                inx++;
            }

            
        %>

    </div>
    
</body>
</html>
