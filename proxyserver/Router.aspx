﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" ErrorPage="~/customError.aspx"
    CodeBehind="Router.aspx.cs" Inherits="proxyserver._Default" %>
<%  
    /*Hardcoded redirection*/
    System.Xml.XmlDocument staticXml = new System.Xml.XmlDocument();
    proxyserver.StaticData staticData = null;
    
    /*Gathering parameters from url route*/
    string application = Page.RouteData.Values["application"] as string;
    string routeType = Page.RouteData.Values["routeType"] as string;
    String arguments = Page.RouteData.Values["arguments"] as string;
    
    /*Testing*/  
    if (application == "waze-il" && routeType == "time" && arguments == "test"){
        //arguments = "from=x:34.8426073400701+y:32.11472668604792&to=x:34.7774843452980+y:32.01527158973557";
            arguments = "32.1147266,34.8426073,32.0152715,34.7774843";
    } else //from San Yesidro to Avalon blvd Los Angeles ALL IN ONE
        if (application == "waze-us" && routeType == "timedist" && arguments == "test") {
            arguments = "32.555556,-117.047043,33.989112,-118.291487";
    } else
        if (application == "waze-us" && routeType == "time" && arguments == "test") {
            arguments = "32.555556,-117.047043,33.989112,-118.291487";
    } else 
        if (application == "waze-il" && routeType == "distance" && arguments == "test"){
            arguments = "32.1147266,34.8426073,32.0152715,34.7774843";
    } else 
        if (application == "waze-il" && routeType == "timedist" && arguments == "test"){
            arguments = "32.1147266,34.8426073,32.0152715,34.7774843";
    } else 
        if (application == "waze-us" && routeType == "distance" && arguments == "test"){
            arguments = "32.555556,-117.047043,33.989112,-118.291487";
    } else 
        if (application == "maps" && routeType == "route" && arguments == "test"){
            arguments = "Alenbi 44, Tel Aviv, Israel; Kiryat Ben Gurion, Jerusalem, Israel";
    } else
        if (application == "revgeocode" && routeType == "country" && arguments == "test"){
            arguments = "32.1147266,34.8426073";
    } else
        if (application == "geocode" && routeType == "a" && arguments == "test")
        {
            arguments = "Alenbi 44, Tel Aviv, Israel";
    } else
        if (application == "waze-row" && routeType == "time" && arguments == "test"){
            arguments = "52.520007,13.404954,52.499295,13.273973";
    } else
        if (application == "waze-row" && routeType == "timedist" && arguments == "test"){
                arguments = "52.520007,13.404954,52.499295,13.273973";
    }
      
    /*loading static data from xml*/
    try
    {
        staticData = new proxyserver.StaticData(Server.MapPath("~/App_Data/routing.xml"), application, routeType, arguments);
    }
    catch (Exception e)
    {
        Response.Write("error:" + e.Message);
        Response.End();
    }
    
    try
    {
        if (staticData != null)
        {
            proxyserver.proxyResponse proxyServer = new proxyserver.proxyResponse(staticData);
            string response = proxyServer.getResponse();
            int response_code = Response.StatusCode;
            Response.Write(response);
            //proxyserver.StaticData.dbInsert(response, response_code, Request, application, routeType, arguments);
        }
    }
    catch (Exception e)
    {
        //proxyserver.StaticData.dbInsert(e.Message, 500, Request, application, routeType, arguments);
        Response.Write("error:"+e.Message);
    }
%>